import React from "react";
import "./Tori.css";
import Radium from 'radium';



const Tori = (props) => {

  const hover = {
    ':hover': {
      backgroundColor: 'lightgreen',
    }
  };

  return (
    
    <div className="inputs">
      <input id="input" type="text" value="Hakusana ja/tai postinumero" />
      <select className="kaikkiOsastot" name="what">
        <option id="1">Kaikki osastot</option>
      </select>
      <select className="kokoSuomi" name="where">
        <option id="1">Koko Suomi</option>
      </select>
     <p> <input style={props.positioning1} type="checkbox" name="how" /> &nbsp; &nbsp; Myydään
      
      <input style={props.positioning2} type="checkbox" name="how"/> &nbsp;&nbsp; Ostetaan
      
      <input style={props.positioning3} type="checkbox" name="how"/> &nbsp; &nbsp; Vuokrataan
      
      <input style={props.positioning4} type="checkbox" name="how"/> &nbsp; Halutaan vuokrata
      
      <input style={props.positioning5} type="checkbox" name="how"/>Annetaan
      
      <span>Tallenna Haku</span> <button style={hover}>Hae</button>

      </p>
    </div>
 
  )
};

export default Radium(Tori);
