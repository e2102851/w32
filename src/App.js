import React from "react";
import tori from "./tori.png";
import Tori from "./Tori";

const positioning1 = {
  position: 'relative',
  left: '18px'
}

const positioning2 = {
  position: 'relative',
  left: '12px'
}

const positioning3 = {
  position: 'relative',
  left: '15px'
}

const positioning4 = {
  position: 'relative',
  left: '9px'
}

const positioning5 = {
  display: 'relative',
  width: '24px'
}


function App() {
  return (
    <div>
      
       <Tori positioning1={positioning1} positioning2={positioning2} positioning3={positioning3} positioning4={positioning4} positioning5={positioning5} />
      <img src={tori} className="App-logo" alt="Tori" />
     
    </div>
  );
}

export default App;
